import { FC } from "react";
import { Routes, Route } from "react-router-dom";

import Home from "./Home/Home";
import Navbar from "./Navbar/Navbar";
import Users from "./Users/Users";
import UsersDetails from "./Users/UsersDetails";

const App: FC<{}> = () => (
	<>
		<Navbar />

		<div className="container">
			<Routes>
				<Route path="/" element={<Home />} />
				<Route path="/users" element={<Users />} />
				<Route path="/users/:id" element={<UsersDetails />} />
				<Route path="/users/:id/:name" element={<UsersDetails />} />
			</Routes>
		</div>
	</>
);

export default App;
