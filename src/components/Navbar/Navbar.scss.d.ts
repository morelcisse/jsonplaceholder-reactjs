export const navbarBrand: string;
export const navLink: string;
export const active: string;
export const breadcrumb: string;
