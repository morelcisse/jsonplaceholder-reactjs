import { useEffect, useState } from "react";
import axios, { AxiosResponse } from "axios";
import { useDispatch, connect } from "react-redux";
import { Link } from "react-router-dom";
import ClipLoader from "react-spinners/ClipLoader";
import Swal from "sweetalert2";

import { AnyAction } from "redux";
import { updateLoading, getUsers, updateUser } from "src/reducers/actions";
import { override, baseUrl, closeModal, counters } from "src/helpers";
import { IState, IUser } from "src/interfaces";
import Modal from "./components/Modal";

import "./Users.scss";

const Users = (props: any) => {
	const { loading, users } = props as IState & { users: IUser[] };
	const [modalUser, setModalUser] = useState<IUser>({} as IUser);
	const [values, setValues] = useState({ name: "" } as IUser);
	const dispatch = useDispatch();
	const handleUser = (user: IUser) => {
		setValues(user as IUser);
		setModalUser(user);
	};
	const handleChange = (evt) => {
		const { value } = evt.target;
		const { name } = evt.target;

		setValues({ ...values, [name]: value });
	};
	const handleUpdate = async () => {
		dispatch(updateLoading("users", true));

		await axios
			.put(`${baseUrl}users/${modalUser.id}`, values)
			.then(async (res: AxiosResponse) => {
				dispatch(updateUser(res.data as IUser) as unknown as AnyAction);

				const swalRes = await Swal.fire({
					title: "Succès",
					text: "L'utilisateur à bien été modifiée avec succès.",
					icon: "success",
					confirmButtonText: "OK",
				});

				if (swalRes.isConfirmed) {
					closeModal();
					setValues({ name: "" } as IUser);
					setModalUser({} as IUser);
				}
			});
	};

	useEffect(() => {
		if (users.length === 0) dispatch(getUsers() as unknown as AnyAction);
	}, []);

	if (loading.users)
		return <ClipLoader loading={loading.users} cssOverride={override} size={50} />;

	return (
		<>
			<div className="stats">
				<div className="row">
					<div className="col">
						Nbr. Utilisateurs (<b>Appt</b>) &nbsp;
						<span className="badge bg-primary rounded-pill">
							{counters(users, "Apt")}
						</span>
					</div>
					<div className="col">
						Nbr. Utilisateurs (<b>Suite</b>) &nbsp;
						<span className="badge bg-primary rounded-pill">
							{counters(users, "Suite")}
						</span>
					</div>
				</div>
			</div>

			<hr />

			<div className="table-responsive">
				<table className="table table-striped users-table">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nom</th>
							<th>Nom d'utilisateur (Abbr)</th>
							<th>Email</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{users &&
							users.map((u: IUser) => {
								const split = u.name.split(" ");
								const name = split.join("-");

								return (
									<tr key={u.id}>
										<td>
											<div>{u.id}</div>
										</td>
										<td>
											<div>{u.name}</div>
										</td>
										<td>
											<div>{u.username}</div>
										</td>
										<td>
											<div>{u.email}</div>
										</td>
										<td className="table-warning">
											<div
												className="btn-group"
												role="group"
												aria-label="Basic example"
											>
												<Link
													type="button"
													className="btn btn-secondary"
													to={`/users/${u.id}/${name}`}
													title="Accueil"
												>
													Détails
												</Link>

												<button
													onClick={() => handleUser(u)}
													data-bs-toggle="modal"
													data-bs-target="#editModal"
													type="button"
													className="btn btn-warning"
												>
													Modifier
												</button>
											</div>
										</td>
									</tr>
								);
							})}
					</tbody>
				</table>
			</div>

			{modalUser && (
				<Modal
					values={values}
					user={modalUser}
					handleChange={handleChange}
					handleUpdate={handleUpdate}
				/>
			)}
		</>
	);
};

const mapStateToProps = (state: IState) => ({
	users: state.users,
	loading: state.loading,
});
const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
