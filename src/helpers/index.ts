import axios from "axios";

import { CSSProperties } from "react";
import { IPost, IUser } from "../interfaces";

export const dev = process.env.NODE_ENV === "development";
export const basename = dev ? "/" : "/jsonplaceholder/";
export const baseUrl = "https://jsonplaceholder.typicode.com/";
export const override = {
	display: "block",
	margin: "0 auto",
	borderColor: "red",
	marginTop: "100px",
} as CSSProperties;

/**
 *
 * @param users
 * @param key
 */
export const counters = (users: IUser[], key: string) =>
	users.filter((user) => user.address.suite.startsWith(key)).length;

/**
 * Close Modal
 */
export const closeModal = () => {
	const modalBackdrop = document.getElementsByClassName("modal-backdrop")[0];
	const closeBtn = document.querySelectorAll(".btn-close");

	document.body.classList.remove("modal-open");
	document.body.style.paddingRight = "initial";
	if (modalBackdrop) {
		modalBackdrop.remove();
	}
	closeBtn.forEach((el) => (el as HTMLElement).click());
};

/**
 * Sleep !!!
 * @param ms
 */
export const delay = (ms: number) =>
	new Promise((resolve) => {
		setTimeout(resolve, ms);
	});

/**
 * Get users posts
 * @param userId
 */
export const getUserPosts = async (userId: number) => {
	const response = await axios.get<IPost[]>(`${baseUrl}posts?userId=${userId}`);
	const posts: IPost[] = response.data;

	return posts;
};

/**
 * Bootstrap trigger
 */
export const initTrigger = (): void => {
	const d = { show: 200, hide: 100 };
	const tooltipList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));

	if (tooltipList.length > 0) {
		tooltipList.map((el) => new (window as any).bootstrap.Tooltip(el, { delay: d }));
	}
};

/**
 * Truncate string
 * @param value
 * @param limit
 * @param after
 */
export const truncate = (value: string, limit: number, after: string) => {
	let content: any = value.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
	content = content.split(" ").slice(0, limit);
	content = content.join(" ") + (after || "");

	return content;
};

/**
 * Uppercase first character of string
 * @param value
 */
export const jsUcfirst = (value: string) => value.charAt(0).toUpperCase() + value.slice(1);
