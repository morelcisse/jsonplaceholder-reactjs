import {
	SET_USERS,
	SET_USER,
	UPDATE_LOADING,
	UPDATE_USER,
	CLEAR_USER,
} from "../constants/action-types";
import { IState, IUser } from "../interfaces";

const initialState: IState = {
	users: [] as IUser[],
	user: {} as IUser,
	loading: {
		users: true,
		user: true,
	},
};

// eslint-disable-next-line default-param-last
const rootReducer = (state = initialState, action: { type: string; payload: any }) => {
	if (action.type === SET_USERS) {
		const { users } = action.payload;

		return { ...state, users };
	}
	if (action.type === UPDATE_LOADING) {
		const { key } = action.payload;
		const { value } = action.payload;
		const loading = { ...state.loading, [key]: value };

		return { ...state, loading };
	}
	if (action.type === SET_USER) {
		const { users } = state;
		let user = users.find((u: IUser) => u.id === action.payload.user.id) as IUser;
		const index = users.findIndex((u: IUser) => u.id === action.payload.user.id);
		const { posts } = action.payload;
		user = Object.assign(user, { posts });
		users[index] = user;

		return { ...state, users, user };
	}
	if (action.type === CLEAR_USER) {
		return { ...state, user: {} };
	}
	if (action.type === UPDATE_USER) {
		const { users } = state;
		const index = users.findIndex((user: IUser) => user.id === action.payload.user.id);

		users[index] = action.payload.user;

		return { ...state, users };
	}

	return state;
};

export default rootReducer;
