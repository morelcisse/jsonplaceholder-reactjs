import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

const store = createStore(require("./index"), applyMiddleware(thunk));

export default store;
