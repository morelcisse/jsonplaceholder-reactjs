export type IPost = {
	userId: number;
	id: number;
	title: string;
	body: string;
};

export type IUser = {
	id: number;
	name: string;
	username: string;
	email: string;
	address: {
		street: string;
		suite: string;
		city: string;
	};
	posts?: IPost[];
};

export interface IState {
	users: IUser[];
	user: IUser;
	loading: {
		[x: string]: boolean;
	};
}
