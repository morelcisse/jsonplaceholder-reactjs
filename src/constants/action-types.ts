export const SET_USERS = "SET_USERS";

export const SET_USER = "SET_USER";

export const CLEAR_USER = "CLEAR_USER";

export const UPDATE_USER = "UPDATE_USER";

export const UPDATE_LOADING = "UPDATE_LOADING";
